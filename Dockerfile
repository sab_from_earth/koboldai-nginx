# Use the official NGINX image as the base image
FROM nginx:alpine

# Set the working directory in the container
WORKDIR /usr/share/nginx/html

RUN apk add --no-cache git && \
    rm -rf .git && \
    git clone https://github.com/LostRuins/lite.koboldai.net.git && \
    mv lite.koboldai.net/* ./ && \
    rm -rf .git lite.koboldai.net && \
    apk del git

# Use the default NGINX port
EXPOSE 80

# Start NGINX and keep the process running in the foreground
CMD ["nginx", "-g", "daemon off;"]
